const crypto = require('crypto')
const fs = require('fs')

const genSeed = (length) => {
    const randomBytes = crypto.randomBytes(length)
    const wordlist = JSON.parse(fs.readFileSync('./wordlist.json'))
    const words = [...randomBytes].map(b => wordlist[b])

    return {
        binary: Buffer.from(randomBytes),
        seedPhrase: words
    }
}

const feed = genSeed(8)
const seed = genSeed(16)

console.log(`https://pastebin.pro/?feed=${feed.seedPhrase.join('-')}&seed=${seed.seedPhrase.join('-')}`)