SELECT id, lower(word) from wordlist
WHERE id < 1000 and word REGEXP '^[a-zA-Z]*$' and length(word) >1
and word not in ('de', 'er', 'sn', 'la', 'ex')
ORDER by length(word), id
LIMIT 256
