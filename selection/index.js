const parse = require('csv-parse')
const fs = require('fs')

const file = fs.readFileSync('./wordlist.csv')
parse(file, {

}, (err, rows) => {
    const wordlist = []
    for (const row of rows) {
        wordlist.push(row[1])
    }
    fs.writeFileSync('wordlist.json', JSON.stringify(wordlist))
})