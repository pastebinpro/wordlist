# Purpose

This is a research on what mnemonic phrases should be chosen as url encoding binary messages.

## Goals

- Shortest url length possible
- word list of reasonable size to reduce loading time

## Solution

Words ranking in length then frequency was chosen.

Word list was acquired from `https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists#Project_Gutenberg`.

They're copy and pasted into LibreOffice Calc, exported into `csv` file, then imported into SQLite database. A query was written to rank the words length first then frequency, with the exeption of removing the arcaic words.

256 words in total was condensed, becasuse 2^8=256, if the word list is 256 in length, then 1 word represents 8 bits, a byte. If the word list length is 512, then 1 word represents 9 bits, more efficient in word encoding length but harder to encode. To achieve 16 bit encoding, 2^16=65536 word list must be loaded, which increases loading time, also the average word length increases, diminishing the efficiency tring to achieve with longer word list.

That should explain why word ranking in length was chosen, but why frequency? Because frequent words put together are easier look at, they seem to form sentences, if the seed is random anyway, it might inspire prophets, as nothing happens randomly.

## Folder stucture

- selection: the files and queries used to condense the words
- encoding: the proof of concept to encode random bytes into mnemonic seed

## Result

Pastebin.pro urls will look something like this:

`https://pastebin.pro/?feed=met-hope-came-thus-door-tom-such-see&seed=old-such-to-done-had-hear-eye-long-upon-look-this-by-top-they-ago-due`

`https://pastebin.pro/?feed=case-eat-me-ago-few-told-says-big&seed=mrs-ten-take-king-say-as-ye-must-our-tom-dear-have-last-did-took-is`

The `feed` is for identifying the encrypted slot, `seed` is for encoding the decryption seed.